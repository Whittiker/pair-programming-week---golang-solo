package main

import "testing"

func TestFizzBuzz(t *testing.T) {
	nums := []int{1, 2, 4}
	for _, num := range nums {
		tester := fizzBuzz(num)
		if tester != num {
			t.Errorf("fizzBuzz was incorrect, it should have produced %d it got %d", num, tester)
		}
	}

	fizzNums := []int{3, 9, 123}
	for _, num := range fizzNums {
		tester := fizzBuzz(num)
		if tester != "Fizz" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "Fizz", tester)
		}
	}

	buzzNums := []int{5, 20, 200}
	for _, num := range buzzNums {
		tester := fizzBuzz(num)
		if tester != "Buzz" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "Buzz", tester)
		}
	}

	fizzBuzzNums := []int{15, 45}
	for _, num := range fizzBuzzNums {
		tester := fizzBuzz(num)
		if tester != "FizzBuzz" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "FizzBuzz", tester)
		}
	}

	popNums := []int{7, 28, 77}
	for _, num := range popNums {
		tester := fizzBuzz(num)
		if tester != "Pop" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "Pop", tester)
		}
	}

	fizzPopNums := []int{21, 63, 126}
	for _, num := range fizzPopNums {
		tester := fizzBuzz(num)
		if tester != "FizzPop" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "FizzPop", tester)
		}
	}

	buzzPopNums := []int{35, 70, 140}
	for _, num := range buzzPopNums {
		tester := fizzBuzz(num)
		if tester != "BuzzPop" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "BuzzPop", tester)
		}
	}

	fizzBuzzPopNums := []int{105, 210, 315}
	for _, num := range fizzBuzzPopNums {
		tester := fizzBuzz(num)
		if tester != "FizzBuzzPop" {
			t.Errorf("fizzBuzz was incorrect, it should have produced %s it got %s", "FizzBuzzPop", tester)
		}
	}
}
