package main

import "fmt"

func main() {
	nums := []int{1, 2, 4, 3, 9, 123, 5, 20, 200, 15, 45, 315, 7, 28, 77, 21, 63, 126, 35, 70, 140, 105, 210, 315}
	for _, num := range nums {
		fizzBuzz(num)
	}
}

func fizzBuzz(num int) interface{} {
	if num%3 == 0 && num%7 == 0 && num%5 == 0 {
		fmt.Println(num, "produces a FizzBuzzPop")
		return "FizzBuzzPop"
	} else if num%3 == 0 && num%5 == 0 {
		fmt.Println(num, "produces a FizzBuzz")
		return "FizzBuzz"
	} else if num%3 == 0 && num%7 == 0 {
		fmt.Println(num, "produces a FizzPop")
		return "FizzPop"
	} else if num%5 == 0 && num%7 == 0 {
		fmt.Println(num, "produces a BuzzPop")
		return "BuzzPop"
	} else if num%5 == 0 {
		fmt.Println(num, "produces a Buzz")
		return "Buzz"
	} else if num%3 == 0 {
		fmt.Println(num, "produces a Fizz")
		return "Fizz"
	} else if num%7 == 0 {
		fmt.Println(num, "produces a Pop")
		return "Pop"
	}
	fmt.Println(num, "produces", num)
	return num
}
